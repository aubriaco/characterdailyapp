package com.mucaroo.characterdailyapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mucaroo.characterdailyapp.R;
import com.mucaroo.characterdailyapp.enums.BlockType;
import com.mucaroo.characterdailyapp.models.Article;
import com.mucaroo.characterdailyapp.models.Block;
import com.mucaroo.characterdailyapp.models.Lesson;

import java.util.List;

/**
 * Created by .Jani on 2/2/2017.
 */

public class HomeAdapter extends ArrayAdapter<Block> {

    public interface OnItemClickCallback {
        void onBlockClick(Block item);
    }

    private OnItemClickCallback mCallback;

    public HomeAdapter(Context context, int resource, List<Block> objects, OnItemClickCallback callback) {
        super(context, resource, objects);
        mCallback = callback;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Block block = getItem(position);

        if (convertView == null) {

            Article article = (Article)block.article;
            if(block.type == BlockType.Quote) {
                Lesson lesson = (Lesson)article;
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_main_item_quote, parent, false);
                TextView caption = (TextView) convertView.findViewById(R.id.caption);
                caption.setText(lesson.quote);
            }
            else if(block.type == BlockType.Prayer)
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_main_item_prayer, parent, false);
            else {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_main_item_lesson, parent, false);
                TextView caption = (TextView)convertView.findViewById(R.id.caption);
                caption.setText(article.title);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mCallback != null)
                        mCallback.onBlockClick(block);
                }
            });
        }

        return convertView;
    }
}
