package com.mucaroo.characterdailyapp.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mucaroo.characterdailyapp.callbacks.AuthResultCallback;
import com.mucaroo.characterdailyapp.callbacks.CreateResultCallback;
import com.mucaroo.characterdailyapp.models.Base;
import com.mucaroo.characterdailyapp.models.ImageInfo;
import com.mucaroo.characterdailyapp.models.Lesson;
import com.mucaroo.characterdailyapp.models.Schedule;
import com.mucaroo.characterdailyapp.models.ScheduleLessonQuote;
import com.mucaroo.characterdailyapp.models.User;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by .Jani on 2/6/2017.
 */

class FirebaseDB extends DB {

    private User mUser;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mDatabase;

    protected FirebaseDB() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public void authenticateUser(String username, String password, final AuthResultCallback callback) {

        mAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    User u = new User();
                    u.id = user.getUid();
                    u.email = user.getEmail();
                    u.name = user.getDisplayName();
                    mUser = u;

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                mFirebaseAuth.removeAuthStateListener(mAuthListener);
            }
        };
        mFirebaseAuth.addAuthStateListener(mAuthListener);

        mFirebaseAuth.signInWithEmailAndPassword(username, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()) {
                    Log.d(TAG, "Failed authentication.");
                    callback.onFail();
                }
                else {
                    Log.d(TAG, "Successful authentication.");
                    FirebaseUser user = mFirebaseAuth.getCurrentUser();
                    User u = new User();
                    u.id = user.getUid();
                    u.email = user.getEmail();
                    u.name = user.getDisplayName();
                    mUser = u;
                    callback.onLogin(u);
                }
            }
        });
    }

    @Override
    public void createUser(String username, String password, final CreateResultCallback callback) {


        mFirebaseAuth.createUserWithEmailAndPassword(username, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()) {
                    Log.d(TAG, "Failed creation.");
                    callback.onFail();
                }
                else {
                    Log.d(TAG, "Created!");
                    callback.onSuccess();
                }
            }
        });
    }

    @Override
    public void getLesson(String id, final DBCallback<Lesson> callback) {

        DatabaseReference ref = mDatabase.getReference("lessons/" + id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Lesson lesson = dataSnapshot.getValue(Lesson.class);
                lesson.rid = dataSnapshot.getKey();
                callback.success(lesson);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void getImage(String id, final DBCallback<ImageInfo> callback) {
        DatabaseReference ref = mDatabase.getReference("files/images/" + id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ImageInfo image = dataSnapshot.getValue(ImageInfo.class);
                image.rid = dataSnapshot.getKey();
                callback.success(image);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void getSchedule(String clientId, final DBCallback<Schedule> callback) {
        DatabaseReference ref = mDatabase.getReference("schedules/" + clientId);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Schedule item = dataSnapshot.getValue(Schedule.class);
                item.rid = dataSnapshot.getKey();
                callback.success(item);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void getScheduleLessonQuote(String clientId, int pillar, int day, String idx, final DBCallback<ScheduleLessonQuote> callback) {
        DatabaseReference ref = mDatabase.getReference("schedules/" + clientId + "/lessons/" + pillar + "/" + day + "/" + idx);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ScheduleLessonQuote item = dataSnapshot.getValue(ScheduleLessonQuote.class);
                item.rid = dataSnapshot.getKey();
                callback.success(item);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void get(final Class c, String id, final DBCallback<Base> callback) {
        DatabaseReference ref = mDatabase.getReference(id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Base result = (Base)dataSnapshot.getValue(c);
                result.rid = dataSnapshot.getKey();
                callback.success(result);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
