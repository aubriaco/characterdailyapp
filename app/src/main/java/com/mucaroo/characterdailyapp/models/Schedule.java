package com.mucaroo.characterdailyapp.models;

import android.util.Log;

import com.mucaroo.characterdailyapp.annotations.DBInclude;

import java.util.ArrayList;
import java.util.Date;

import static java.lang.Math.abs;

/**
 * Created by jinxi on 2/27/2017.
 */

public class Schedule extends Base {

    @DBInclude
    public ArrayList<Long> pillars;

    @DBInclude
    public ArrayList<ArrayList<String> > behaviors;

    public SchedulePillarDay getClosestPillar() {
        int pillar = 0, n = 0;
        Date now = new Date();
        long nowt = now.getTime();
        long closest = -1 * nowt;
        for(Long t : pillars) {
            long diff = (t*1000) - nowt;
            Log.d("SCHEDULE", "PILLAR: " + new Date(t*1000).toString() + " VERSUS " + now.toString() + " DIFF: " + diff );
            if(diff > closest && diff <= 0) {
                closest = diff;
                pillar = n;
            }
            n++;
        }

        long day = (-1 * closest) / (1000 * 60 * 60 * 24);

        Log.d("Schedule", "DAY: " + day);

        return new SchedulePillarDay(pillar, (int)day);
    }
}
