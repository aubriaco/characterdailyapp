package com.mucaroo.characterdailyapp.models;

import com.mucaroo.characterdailyapp.annotations.DBInclude;

/**
 * Created by .Jani on 2/2/2017.
 */

public class Article extends Base {
    @DBInclude
    public int pillar, grade;
    @DBInclude
    public String title, body, image;
}
