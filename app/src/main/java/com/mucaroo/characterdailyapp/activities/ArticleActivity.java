package com.mucaroo.characterdailyapp.activities;

import android.os.Bundle;
import android.util.Log;

import com.mucaroo.characterdailyapp.R;
import com.mucaroo.characterdailyapp.data.DBCallback;
import com.mucaroo.characterdailyapp.models.ImageInfo;
import com.mucaroo.characterdailyapp.models.Lesson;
import com.mucaroo.characterdailyapp.util.Utility;

public class ArticleActivity extends BaseActivity {
    private static final String TAG = "ArticleActivity";

    public static Lesson mCurrentArticle;

    public String mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        setTitle(R.string.daily_lesson);


        mContent = Utility.getAssetAsString(this, "templates/article.html");

        mContent = mContent.replace("$(title)", mCurrentArticle.title);
        mContent = mContent.replace("$(quote)", mCurrentArticle.quote);
        mContent = mContent.replace("$(body)", mCurrentArticle.body);



        Utility.getImage(this, mCurrentArticle.image, new DBCallback<ImageInfo>() {
            @Override
            public void success(ImageInfo o) {
                Log.d(TAG, o.url);
                mContent = mContent.replace("$(image)", o.url);
                getWebView().loadData(mContent, "text/html; charset=UTF-8", null);
            }
        });



    }
}
