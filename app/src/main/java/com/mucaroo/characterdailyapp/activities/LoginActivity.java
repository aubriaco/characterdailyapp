package com.mucaroo.characterdailyapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mucaroo.characterdailyapp.R;
import com.mucaroo.characterdailyapp.callbacks.AuthResultCallback;
import com.mucaroo.characterdailyapp.data.DB;
import com.mucaroo.characterdailyapp.models.User;
import com.mucaroo.characterdailyapp.util.Utility;

public class LoginActivity extends BaseActivity {

    public static final String TAG = "LoginActivity";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
            {
                if(resultCode == 1) {
                    String email = data.getStringExtra("email");
                    String password = data.getStringExtra("password");
                    getEmailText().setText(email);
                    getPasswordText().setText(password);
                    login();
                }
                break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        User user = Utility.getUser(this);
        if(user != null) {
            login(false, user.email, user.password);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            return;
        }

        getLoginButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        getRegisterButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
    }

    private void register() {
        Intent in = new Intent(this, RegisterActivity.class);
        startActivityForResult(in, 1);
    }

    private void login() {
        login(true,null,null);
    }

    private void login(final boolean alerts, String email, String password) {

        if(alerts) {
            email = getEmailText().getText().toString();
            password = getPasswordText().getText().toString();
            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Please fill in email and password.", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        final String fpassword = password;

        DB.getInstance(this).authenticateUser(email, password, new AuthResultCallback() {
            @Override
            public void onLogin(User user) {
                Log.d(TAG, "User logged in!");
                if(alerts) {
                    getEmailText().setText("");
                    getPasswordText().setText("");
                    user.password = fpassword;
                    Utility.storeUser(LoginActivity.this, user);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }
            }

            @Override
            public void onFail() {
                Log.d(TAG, "Failed login.");
                if(alerts)
                    Utility.showAlert(LoginActivity.this, "Failed login.", "Invalid email/password.");
            }
        });
    }

}
