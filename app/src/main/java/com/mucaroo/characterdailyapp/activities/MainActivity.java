package com.mucaroo.characterdailyapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.mucaroo.characterdailyapp.R;
import com.mucaroo.characterdailyapp.adapters.HomeAdapter;
import com.mucaroo.characterdailyapp.data.DB;
import com.mucaroo.characterdailyapp.data.DBCallback;
import com.mucaroo.characterdailyapp.enums.BlockType;
import com.mucaroo.characterdailyapp.models.Base;
import com.mucaroo.characterdailyapp.models.Block;
import com.mucaroo.characterdailyapp.models.Lesson;
import com.mucaroo.characterdailyapp.models.Schedule;
import com.mucaroo.characterdailyapp.models.ScheduleLessonQuote;
import com.mucaroo.characterdailyapp.models.SchedulePillarDay;
import com.mucaroo.characterdailyapp.util.Globals;

import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends BaseActivity implements HomeAdapter.OnItemClickCallback {
    public static final String TAG = "MainActivity";


    protected HomeAdapter mAdapter;
    protected ArrayList<Block> mBlocks = new ArrayList<>();
    protected SchedulePillarDay mSchedulePillarDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("LOADING...");


        DB.getInstance(this).getSchedule(Globals.ClientID, new DBCallback<Schedule>() {
                    @Override
                    public void success(Schedule o) {

                        mSchedulePillarDay = o.getClosestPillar();

                        setTitle(Globals.PillarNames[mSchedulePillarDay.pillar]);

                        DB.getInstance(MainActivity.this).getScheduleLessonQuote(Globals.ClientID, mSchedulePillarDay.pillar, mSchedulePillarDay.day, "0", new DBCallback<ScheduleLessonQuote>() {
                            @Override
                            public void success(ScheduleLessonQuote o) {
                                loadLesson(o.lesson);
                            }
                        });

                    }
                });



    }


    private void loadLesson(String id) {
        DB.getInstance(this).getLesson(id, new DBCallback<Lesson>() {
            @Override
            public void success(Lesson o) {
                mBlocks.add(new Block(BlockType.Article, o));
                mBlocks.add(new Block(BlockType.Quote, o));
                updateAdapter();
            }
        });

    }

    private void updateAdapter() {
        mAdapter = new HomeAdapter(this, 0, mBlocks, this);
        getMainList().setAdapter(mAdapter);
    }


    @Override
    public void onBlockClick(Block item) {

         switch (item.type) {
             case Article:
             {
                 ArticleActivity.mCurrentArticle = (Lesson)item.article;
                 Intent in = new Intent(this, ArticleActivity.class);
                 startActivity(in);
                 break;
             }
         }
    }
}
